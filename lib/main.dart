import 'package:beachcounter/models/appstate.dart';
import 'package:beachcounter/pages/games.dart';
import 'package:beachcounter/pages/settings.dart';
import 'package:beachcounter/pages/scores.dart';
import 'package:beachcounter/pages/home.dart';
import 'package:beachcounter/pages/players.dart';
import 'package:beachcounter/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => AppState(),
      child: const BeachCounter(),
    ),
  );
}

class BeachCounter extends StatelessWidget {
  const BeachCounter({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Beach Counter',
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      theme: const MaterialTheme(TextTheme()).light(),
      darkTheme: const MaterialTheme(TextTheme()).dark(),
      themeMode: ThemeMode.light, // Default is system
      initialRoute: '/',
      routes: {
        '/': (context) => const HomePage(),
        '/scores': (context) => const Scoreboard(),
        '/prefs': (context) => const SettingsPage(),
        '/players': (context) => const PlayersPage(),
        '/games': (context) => const Games(),
      },
    );
  }
}
