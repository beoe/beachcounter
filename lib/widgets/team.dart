import 'dart:io';

import 'package:beachcounter/models/appstate.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vibration/vibration.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../models/player.dart';

class TeamWidget extends StatelessWidget {
  final int teamIdx;
  final TextStyle _playerStyle = const TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 18,
  );
  TeamWidget({super.key, required this.teamIdx});

  @override
  Widget build(BuildContext context) {
    return Consumer<AppState>(builder: (context, state, child) {
      return GestureDetector(
        onTap: () async {
          if (state.vibrateOnPoint && (Platform.isAndroid || Platform.isIOS)) {
            await Vibration.vibrate(duration: 200);
          }
          if (state.servingTeam != teamIdx) {
            state.servingTeam = teamIdx;
            state.teams[teamIdx].incrementServingPlayer();
          }
          var swap = state.incrementScore(teamIdx);
          if (state.teams[teamIdx].score >= 21 &&
              state.teams[teamIdx].score - state.teams[1 - teamIdx].score >=
                  2) {
            state.gameOver = true;
            state.winningTeam = teamIdx;
          }
          if (swap && context.mounted) {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text(
                        AppLocalizations.of(context)?.warning ?? 'Warning'),
                    content: Text(AppLocalizations.of(context)?.sidesSwapped ??
                        'Sides swapped'),
                    actions: <Widget>[
                      TextButton(
                        style: TextButton.styleFrom(
                          textStyle: Theme.of(context).textTheme.labelLarge,
                        ),
                        child: Text(AppLocalizations.of(context)?.ok ?? 'OK'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                });
          }
        },
        onLongPress: () async {
          if (state.servingTeam != teamIdx) {
            return;
          }
          if (state.vibrateOnPoint && (Platform.isAndroid || Platform.isIOS)) {
            await Vibration.vibrate(duration: 600);
          }
          state.decrementScore(teamIdx);
        },
        child: Container(
          padding: const EdgeInsets.all(16),
          decoration: (teamIdx == state.servingTeam)
              ? BoxDecoration(
                  border: Border.all(
                      width: 6, color: Theme.of(context).colorScheme.primary),
                  color: Theme.of(context).colorScheme.secondaryContainer,
                )
              : BoxDecoration(
                  border: Border.all(width: 4, color: Colors.transparent),
                  color: Theme.of(context).colorScheme.background,
                ),
          child: Column(
            children: <Widget>[
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                DecoratedBox(
                  decoration: BoxDecoration(
                      color: (teamIdx == state.servingTeam)
                          ? Theme.of(context).colorScheme.secondaryContainer
                          : Theme.of(context).colorScheme.background),
                  child: Text(state.teams[teamIdx].name,
                      style: Theme.of(context).textTheme.headlineMedium),
                ),
                const SizedBox(width: 10),
                ElevatedButton.icon(
                  style: ButtonStyle(
                      padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                          const EdgeInsets.only(left: 5, right: 0)),
                      backgroundColor: MaterialStateProperty.all<Color>(
                          Theme.of(context).colorScheme.secondaryContainer),
                      foregroundColor: MaterialStateProperty.all<Color>(
                          Theme.of(context).colorScheme.onSecondaryContainer)),
                  onPressed: () {
                    state.teams[teamIdx].swapPlayers();
                  },
                  icon: const Icon(Icons.swap_vert),
                  label: Container(),
                )
              ]),
              const SizedBox(height: 20),
              ...state.teams[teamIdx].players.map((player) {
                return _generatePlayerName(context, player, state);
              }),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      state.teams[teamIdx].score.toString(),
                      style: Theme.of(context).textTheme.displayLarge,
                      textScaleFactor: 1.4,
                    ),
                    (state.showInlineCount)
                        ? inlineScore(context, state, teamIdx)
                        : Container(),
                    (state.showTimeFromLastPoint)
                        ? Text(_formatDuration(
                            context,
                            state.teams[teamIdx].lastPointTime,
                            state.teams[teamIdx].score))
                        : Container(),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    });
  }

  Widget _generatePlayerName(
      BuildContext context, Player player, AppState state) {
    var playerIsServing = (state.servingTeam == teamIdx &&
        state.teams[teamIdx].servingPlayerIndex == player.idx);
    var button = ElevatedButton.icon(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>((playerIsServing)
            ? Theme.of(context).colorScheme.primary
            : Theme.of(context).colorScheme.secondaryContainer),
        foregroundColor: MaterialStateProperty.all<Color>((playerIsServing)
            ? Colors.white
            : Theme.of(context).colorScheme.primary),
        padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
            const EdgeInsets.only(left: 10, right: 10)),
      ),
      onPressed: () async {
        var newPlayer = await _showTextInputDialog(
            context, state.getPlayersNotInTeam(teamIdx), player.name);
        if (context.mounted) {
          if (newPlayer != null && newPlayer != '') {
            state.addPlayer(newPlayer);
            player.setName(newPlayer);
          }
        }
        debugPrint("done with new playername: ${state.teams[teamIdx]}");
      },
      onLongPress: () async {
        if (teamIdx != state.servingTeam ||
            player.idx == state.teams[teamIdx].servingPlayerIndex) {
          return;
        }
        var swapServe = await swapServeDialog(context, state);
        if (swapServe != null && swapServe) {
          state.teams[teamIdx].setServingPlayer(player.idx);
        }
      },
      icon: playerIsServing
          ? const SizedBox(
              height: 55,
              child: Icon(
                Icons.sports_volleyball,
                size: 35,
              ))
          : Container(height: 55),
      label: (player.name == '')
          ? Text(
              "${AppLocalizations.of(context)?.player} ${player.idx + 1}",
              style: _playerStyle,
              overflow: TextOverflow.ellipsis,
            )
          : Text(
              player.name,
              style: _playerStyle,
              overflow: TextOverflow.ellipsis,
            ),
    );
    var b = Badge.count(
      count: (state.initialServingTeam == teamIdx && player.idx == 0)
          ? player.idx + 1
          : (state.initialServingTeam == teamIdx && player.idx == 1)
              ? player.idx + 2
              : (state.initialServingTeam != teamIdx && player.idx == 0)
                  ? player.idx + 2
                  : player.idx + 3,
      isLabelVisible: (state.initialServingTeam >= 0) ? true : false,
      largeSize: 20,
      padding: const EdgeInsets.only(left: 6, right: 6),
      offset: const Offset(10, -5),
      textStyle: const TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.bold,
        fontSize: 14,
      ),
      child: button,
    );
    return Padding(padding: const EdgeInsets.all(7), child: b);
  }

  String _formatDuration(BuildContext context, DateTime lastTime, int score) {
    Duration diff = DateTime.now().difference(lastTime);
    if (score == 0) {
      return AppLocalizations.of(context)?.noPointsYet ?? 'No points yet';
    }
    if (diff.inSeconds > 60) {
      return '${diff.inMinutes} min';
    }
    return '${diff.inSeconds} s';
  }

  final TextEditingController _textFieldController = TextEditingController();
  Future<String?> _showTextInputDialog(
      BuildContext context, List<String> allPlayers, String name) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title:
                Text(AppLocalizations.of(context)?.playerName ?? 'Player name'),
            content: Autocomplete<String>(
              optionsBuilder: (TextEditingValue textEditingValue) {
                debugPrint('textEditingValue: ${textEditingValue.text}');
                if (textEditingValue.text == '') {
                  return allPlayers;
                }
                return allPlayers.where((String option) {
                  debugPrint('option: ${option.toString().toLowerCase()}');
                  return option
                      .toString()
                      .toLowerCase()
                      .contains(textEditingValue.text.toLowerCase());
                });
              },
              fieldViewBuilder: (BuildContext context,
                  TextEditingController textEditingController,
                  FocusNode focusNode,
                  VoidCallback onFieldSubmitted) {
                textEditingController.text = name;
                _textFieldController.text = name;
                return TextFormField(
                  textCapitalization: TextCapitalization.words,
                  controller: textEditingController,
                  autofocus: true,
                  focusNode: focusNode,
                  decoration: InputDecoration(
                    suffixIcon: IconButton(
                      onPressed: textEditingController.clear,
                      icon: const Icon(Icons.clear),
                    ),
                  ),
                  onFieldSubmitted: (String value) {
                    debugPrint('onFieldSubmitted: $value');
                    _textFieldController.text = value;
                    onFieldSubmitted();
                  },
                  onChanged: (value) => _textFieldController.text = value,
                );
              },
              onSelected: (String selection) {
                debugPrint('You just selected $selection');
                String newPlayer = selection;
                Navigator.pop(context, newPlayer);
              },
            ),
            actions: <Widget>[
              ElevatedButton(
                child: Text(AppLocalizations.of(context)?.cancel ?? 'Cancel'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              ElevatedButton(
                  child: const Text('OK'),
                  onPressed: () {
                    debugPrint(
                        'You just selected ${_textFieldController.text}');
                    String newPlayer = _textFieldController.text;
                    Navigator.pop(context, newPlayer);
                    _textFieldController.clear();
                  }),
            ],
          );
        });
  }

  swapServeDialog(BuildContext context, AppState state) async {
    var swapServe = await showDialog<bool>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title:
                Text(AppLocalizations.of(context)?.swapServe ?? 'Swap serve'),
            content: Text(AppLocalizations.of(context)?.swapServeContent ??
                'Swap serve with other player?'),
            actions: <Widget>[
              TextButton(
                style: TextButton.styleFrom(
                  textStyle: Theme.of(context).textTheme.labelLarge,
                ),
                child: Text(AppLocalizations.of(context)?.cancel ?? 'Cancel'),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              TextButton(
                style: TextButton.styleFrom(
                  textStyle: Theme.of(context).textTheme.labelLarge,
                ),
                child: Text(AppLocalizations.of(context)?.ok ?? 'OK'),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
            ],
          );
        });
    return swapServe;
  }

  Widget inlineScore(BuildContext context, AppState state, int teamIdx) {
    List<TextSpan> children = [];
    for (var score in state.scores) {
      if (score.team == teamIdx) {
        children.add(const TextSpan(
            text: 'X ',
            style: TextStyle(
                fontWeight: FontWeight.bold, fontFamily: "RobotoMono")));
      } else {
        children.add(const TextSpan(
          text: '- ',
          style: TextStyle(
              fontWeight: FontWeight.normal, fontFamily: "RobotoMono"),
        ));
      }
      if (score.manualCorrection && children.length > 1) {
        children.removeLast();
        // remove last score of the this team, if manual correction is enabled
        for (int i = children.length - 1; i >= 0; --i) {
          if (children[i].text == 'X ' && score.team == teamIdx) {
            children.removeAt(i);
            break;
          } else if (children[i].text == '- ' && score.team != teamIdx) {
            children.removeAt(i);
            break;
          }
        }
      }
    }
    var textResult = Text.rich(TextSpan(children: children));
    return textResult;
  }
}
