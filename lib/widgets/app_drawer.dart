import 'package:beachcounter/models/appstate.dart';
import 'package:beachcounter/pages/games.dart';
import 'package:beachcounter/pages/players.dart';
import 'package:beachcounter/pages/scores.dart';
import 'package:beachcounter/pages/settings.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AppDrawer extends StatelessWidget {
  const AppDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    var appState = Provider.of<AppState>(context, listen: true);
    return Drawer(
        child: ListView(
      padding: EdgeInsets.zero,
      children: [
        DrawerHeader(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/volley.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: Container(),
        ),
        ListTile(
          leading: const Icon(Icons.list),
          title: Text(AppLocalizations.of(context)?.scoreBoard ?? 'Scoreboard'),
          onTap: () {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => const Scoreboard()));
          },
        ),
        ListTile(
          leading: const Icon(Icons.people),
          title: Text(AppLocalizations.of(context)?.players ?? 'Players'),
          onTap: () {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => const PlayersPage()));
          },
        ),
        (appState.enableSaveGame)
            ? ListTile(
                leading: const Icon(Icons.list),
                title: Text(
                    AppLocalizations.of(context)?.savedGames ?? 'Saved Games'),
                onTap: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => const Games()));
                },
              )
            : Container(),
        ListTile(
          leading: const Icon(Icons.settings),
          title: Text(AppLocalizations.of(context)?.settings ?? 'Settings'),
          onTap: () {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => const SettingsPage()));
          },
        ),
      ],
    ));
  }
}
