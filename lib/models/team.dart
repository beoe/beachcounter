import 'package:beachcounter/models/player.dart';
import 'package:flutter/material.dart';

class Team {
  int id;
  final String name;
  int score = 0;
  int _servingPlayerIndex = 0;
  bool initialServe = true;
  DateTime _lastPointTime = DateTime.now();
  final Color color;
  final List<Player> players;

  Team({
    required this.id,
    required this.name,
    required this.players,
    required this.color,
  });

  Player getServingPlayer() {
    return players[_servingPlayerIndex];
  }

  String getFullTeamName() {
    var p = players.map((p) => p.name).toList().join("/");
    return "$name - $p";
  }

  int get servingPlayerIndex => _servingPlayerIndex;

  DateTime get lastPointTime => _lastPointTime;

  void incrementServingPlayer() {
    if (initialServe) {
      initialServe = !initialServe;
      _servingPlayerIndex = 0;
      return;
    }
    _servingPlayerIndex = (_servingPlayerIndex + 1) % players.length;
  }

  void swapPlayers() {
    final temp = players[0];
    players[0] = players[1];
    players[0].setIdx(0);
    players[1] = temp;
    players[1].setIdx(1);
  }

  void setServingPlayer(int idx) {
    _servingPlayerIndex = idx;
  }

  void incrementScore() {
    score++;
    _lastPointTime = DateTime.now();
  }

  void decrementScore() {
    score--;
    if (score < 0) {
      score = 0;
    }
  }
}
