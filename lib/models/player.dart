class Player {
  String name = '';
  int idx;
  bool hasNameSet = false;
  Player({
    required this.idx,
  });

  void setName(n) {
    if (n == '') {
      return;
    }
    name = n;
    hasNameSet = true;
  }

  void setIdx(i) {
    idx = i;
  }

  @override
  String toString() {
    return 'Player{idx: $idx, name: $name}';
  }
}
