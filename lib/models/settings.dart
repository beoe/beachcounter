import 'dart:convert';

Settings settingsFromJson(String str) => Settings.fromJson(json.decode(str));
Settings defaultSettings() => Settings(
      swapAllSeven: false,
      enableSaveGame: false,
      useOnline: false,
      showTimeFromLastPoint: true,
      showInlineCount: true,
      vibrateOnPoint: false,
      showManualSideSwap: false,
    );
String settingsToJson(Settings settings) => json.encode(settings.toJson());

class Settings {
  bool useOnline;
  bool swapAllSeven;
  bool enableSaveGame;
  bool vibrateOnPoint;
  bool showTimeFromLastPoint;
  bool showInlineCount;
  bool showManualSideSwap;
  Settings({
    required this.swapAllSeven,
    required this.useOnline,
    required this.enableSaveGame,
    required this.vibrateOnPoint,
    required this.showTimeFromLastPoint,
    required this.showInlineCount,
    required this.showManualSideSwap,
  });
  factory Settings.fromJson(Map<String, dynamic> json) => Settings(
        useOnline: json["useOnline"] ?? false,
        swapAllSeven: json["swapAllSeven"] ?? false,
        enableSaveGame: json["enableSaveGame"] ?? false,
        vibrateOnPoint: json["vibrateOnPoint"] ?? false,
        showTimeFromLastPoint: json["showTimeFromLastPoint"] ?? true,
        showInlineCount: json["showInlineCount"] ?? true,
        showManualSideSwap: json["showManualSideSwap"] ?? false,
      );
  Map<String, dynamic> toJson() => {
        "useOnline": useOnline,
        "swapAllSeven": swapAllSeven,
        "enableSaveGame": enableSaveGame,
        "vibrateOnPoint": vibrateOnPoint,
        "showTimeFromLastPoint": showTimeFromLastPoint,
        "showInlineCount": showInlineCount,
        "showManualSideSwap": showManualSideSwap,
      };

  @override
  String toString() {
    return 'Score{useOnline: $useOnline}';
  }
}
