import 'dart:convert';

Score scoreFromJson(String str) => Score.fromJson(json.decode(str));
String scoreToJson(Score score) => json.encode(score.toJson());

class Score {
  int team;
  String clientId;
  String gameId;
  String ts;
  String servingPlayer;
  int servingPlayerIndex;
  int scoreA;
  int scoreB;
  bool isSideout;
  bool manualCorrection;
  Score({
    required this.team,
    required this.clientId,
    required this.gameId,
    required this.ts,
    required this.servingPlayer,
    required this.servingPlayerIndex,
    required this.scoreA,
    required this.scoreB,
    required this.isSideout,
    required this.manualCorrection,
  });

  Map<String, dynamic> toDatabase() {
    return {
      'team': team,
      'clientId': clientId,
      'ts': ts,
      'servingPlayer': servingPlayer,
      'servingPlayerIndex': servingPlayerIndex,
      'gameId': gameId,
      'scoreA': scoreA,
      'scoreB': scoreB,
      'isSideout': isSideout,
      'manualCorrection': manualCorrection,
    };
  }

  factory Score.fromJson(Map<String, dynamic> json) => Score(
        team: json["team"],
        clientId: json["clientId"],
        ts: json["ts"],
        servingPlayer: json["servingPlayer"],
        servingPlayerIndex: json["servingPlayerIndex"],
        gameId: json["gameId"],
        scoreA: json["scoreA"],
        scoreB: json["scoreB"],
        isSideout: json["isSideout"],
        manualCorrection: json["manualCorrection"],
      );

  Map<String, dynamic> toJson() => {
        "team": team,
        "clientId": clientId,
        "ts": ts,
        "servingPlayer": servingPlayer,
        "servingPlayerIndex": servingPlayerIndex,
        "gameId": gameId,
        "scoreA": scoreA,
        "scoreB": scoreB,
        "isSideout": isSideout,
        "manualCorrection": manualCorrection,
      };

  @override
  String toString() {
    return 'Score{team: $team, clientId: $clientId}';
  }
}
