import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:beachcounter/models/player.dart';
import 'package:beachcounter/models/score.dart';
import 'package:beachcounter/models/settings.dart';
import 'package:beachcounter/models/team.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class AppState with ChangeNotifier {
  AppState() {
    _initSettings();
    // initSupabase();
  }

  _initSettings() async {
    log('initSettings in appstate', name: 'AppState/_initSettings');
    _deviceId = await _getId();
    resetGame();
    await _loadPrefs();
    Timer.periodic(const Duration(seconds: 1), (Timer t) => _getTime());
  }

  void _getTime() {
    _playTime = '${DateTime.now().difference(_startTime).inMinutes} min';
    notifyListeners();
  }

  Future<void> _loadPrefs() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String settingsStr = prefs.getString("settings") ?? '{}';
    _settings = settingsFromJson(settingsStr);
    var players = prefs.getStringList('players') ?? [];
    for (var p in players) {
      _allPlayers.add(p);
    }
  }

  // _initState() async {}
  Settings _settings = defaultSettings();
  final List<Team> _teams = [
    Team(
        id: 0,
        name: 'A',
        players: [Player(idx: 0), Player(idx: 1)],
        color: Colors.white),
    Team(
        id: 1,
        name: 'B',
        players: [Player(idx: 0), Player(idx: 1)],
        color: Colors.white)
  ];
  int _servingTeam = -1;
  int _initialServingTeam = -1;
  DateTime _startTime = DateTime.now();
  bool _gameOver = false;
  bool _gameOn = false;
  bool _enableSaveGame = false;
  int _winningTeam = -1;
  String _playTime = '';
  String _gameId = '';
  String _deviceId = '';
  List<String> _allPlayers = [];
  List<Score> _scores = [];
  // late final SupabaseClient? _client;
  bool get gameOver => _gameOver;
  bool get enableSaveGame => _enableSaveGame;
  int get winningTeam => _winningTeam;
  bool get useOnline => _settings.useOnline;
  bool get swapAllSeven => _settings.swapAllSeven;
  bool get vibrateOnPoint => _settings.vibrateOnPoint;
  bool get showManualSideSwap => _settings.showManualSideSwap;
  // bool get isConnected => _client != null;
  bool get gameOn => _gameOn;
  bool get showTimeFromLastPoint => _settings.showTimeFromLastPoint;
  bool get showInlineCount => _settings.showInlineCount;
  String get playTime => _playTime;
  String get gameId => _gameId;
  int get servingTeam => _servingTeam;
  int get initialServingTeam => _initialServingTeam;
  List<Team> get teams => _teams;
  List<Score> get scores => _scores;
  List<String> get allPlayers => _allPlayers;

  set useOnline(val) {
    _settings.useOnline = val;
    saveSettings();
    notifyListeners();
  }

  set enableSaveGame(val) {
    _enableSaveGame = val;
    notifyListeners();
  }

  set initialServingTeam(val) {
    _initialServingTeam = val;
    notifyListeners();
  }

  set winningTeam(val) {
    _winningTeam = val;
    notifyListeners();
  }

  set gameOver(val) {
    _gameOver = val;
    notifyListeners();
  }

  set gameOn(val) {
    _gameOn = val;
    notifyListeners();
  }

  set showManualSideSwap(val) {
    _settings.showManualSideSwap = val;
    saveSettings();
    notifyListeners();
  }

  set swapAllSeven(val) {
    _settings.swapAllSeven = val;
    saveSettings();
    notifyListeners();
  }

  set vibrateOnPoint(val) {
    _settings.vibrateOnPoint = val;
    saveSettings();
    notifyListeners();
  }

  set showTimeFromLastPoint(val) {
    _settings.showTimeFromLastPoint = val;
    saveSettings();
    notifyListeners();
  }

  set showInlineCount(val) {
    _settings.showInlineCount = val;
    saveSettings();
    notifyListeners();
  }

  Future<void> saveGame() async {
    if (!_gameOn) {
      return;
    }
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<Map<String, dynamic>> scores = [];
    for (var s in _scores) {
      scores.add(s.toJson());
    }
    String scoresStr = jsonEncode(scores);
    debugPrint('scoresStr: $scoresStr');
    prefs.setString('game-$_gameId', scoresStr);
    List<String> games = prefs.getStringList('games') ?? [];
    debugPrint('games: $games');
    games.add('${_startTime.toIso8601String()}-$_gameId');
    prefs.setStringList("games", games);
  }

  Future<List<String>> getGames() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> games = prefs.getStringList('games') ?? [];
    return games;
  }

  void saveSettings() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("settings", settingsToJson(_settings));
  }

  Score getScore(int team, bool manualCorrection) {
    var sideout = true;
    if (_scores.isNotEmpty) {
      if (_scores[_scores.length - 1].team == team) {
        sideout = false;
      }
    }
    Score score = Score(
        team: team,
        ts: DateTime.now().toIso8601String(),
        clientId: _deviceId,
        gameId: _gameId,
        servingPlayer: teams[team].getServingPlayer().name,
        servingPlayerIndex: teams[team].getServingPlayer().idx,
        scoreA: _teams[0].score,
        scoreB: _teams[1].score,
        isSideout: sideout,
        manualCorrection: manualCorrection);
    return score;
  }

  // void _updateScoreOnline(int team) async {
  //   if (_deviceId == '') {
  //     _deviceId = await _getId();
  //   }
  //   var score = getScore(team);
  //   if (_client != null && useOnline) {
  //     await _client!.from('beachen').insert(score.toDatabase());
  //   }
  // }

  Future<String> _getId() async {
    if (kIsWeb) {
      var uuid = const Uuid();
      return 'random-${uuid.v4()}';
    }
    var deviceInfoPlugin = DeviceInfoPlugin();
    // final deviceInfo = await deviceInfoPlugin.deviceInfo;
    // final allInfo = deviceInfo.data;
    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfoPlugin.iosInfo;
      return 'ios-${iosDeviceInfo.identifierForVendor}';
    } else if (Platform.isAndroid) {
      var androidDeviceInfo = await deviceInfoPlugin.androidInfo;
      return 'android-${androidDeviceInfo.id}';
    } else if (Platform.isLinux) {
      var linuxDeviceInfo = await deviceInfoPlugin.linuxInfo;
      return 'linux-${linuxDeviceInfo.machineId}';
    }
    var uuid = const Uuid();
    return 'random-${uuid.v4()}';
  }

  void decrementScore(team) {
    _teams[team].decrementScore();
    // if (useOnline) {
    //   _updateScoreOnline(team);
    // }
    var score = getScore(team, true);
    _scores.add(score);
    notifyListeners();
  }

  bool incrementScore(team) {
    // initial point
    if (_gameOn == false) {
      _gameOn = true;
      _startTime = DateTime.now();
      _initialServingTeam = team;
    }
    _teams[team].incrementScore();
    if (useOnline) {
      // _updateScoreOnline(team);
    }
    var score = getScore(team, false);
    _scores.add(score);
    if (swapAllSeven) {
      if ((score.scoreA + score.scoreB) % 7 == 0) {
        swapTeams();
        return true;
      }
    }
    notifyListeners();
    return false;
  }

  set servingTeam(int teamIdx) {
    _servingTeam = teamIdx;
    notifyListeners();
  }

  swapServingTeam() {
    if (_servingTeam == 0) {
      _servingTeam = 1;
    } else {
      _servingTeam = 0;
    }
    notifyListeners();
  }

  Team _initTeam(int team) {
    var t = (team == 0) ? 'A' : 'B';
    return Team(
        id: team,
        name: t,
        players: [Player(idx: 0), Player(idx: 1)],
        color: Colors.white);
  }

  void removePlayerAt(int idx) {
    _allPlayers.removeAt(idx);
    saveAllPlayers();
    notifyListeners();
  }

  void addPlayer(String name) {
    Set<String> playerList = Set.from(_allPlayers);
    playerList.add(name);
    _allPlayers = playerList.toList();
    saveAllPlayers();
    notifyListeners();
  }

  void setAllPlayers(List<String> p) {
    _allPlayers = p;
    saveAllPlayers();
  }

  Future<void> saveAllPlayers() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList('players', _allPlayers);
  }

  void swapTeams() {
    final Team first = _teams.removeAt(0);
    _teams.add(first);
    swapServingTeam();
    notifyListeners();
  }

  List<String> getPlayersNotInTeam(int teamIdx) {
    List<String> result = [...allPlayers];
    var t = teams[teamIdx];
    List<String> names = [];
    for (var element in t.players) {
      names.add(element.name);
    }
    result.removeWhere((p) => names.contains(p));
    return result;
  }

  void resetGame({bool keepPlayers = false}) {
    _gameOver = false;
    _gameId = const Uuid().v4();
    _gameOn = false;
    for (var i = 0; i < 2; i++) {
      if (keepPlayers) {
        List<String> names = [];
        for (var j = 0; j < 2; j++) {
          names.add(_teams[i].players[j].name);
        }
        _teams[i] = _initTeam(i);
        for (var j = 0; j < 2; j++) {
          _teams[i].players[j].setName(names[j]);
        }
      } else {
        _teams[i] = _initTeam(i);
      }
    }
    _scores = [];
    _servingTeam = -1;
    _initialServingTeam = -1;

    notifyListeners();
  }
}
