import 'package:beachcounter/models/appstate.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Games extends StatelessWidget {
  const Games({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${AppLocalizations.of(context)?.savedGames}'),
        leading: IconButton(
            icon: const Icon(Icons.chevron_left),
            onPressed: () => Navigator.pop<void>(context)),
        actions: const <Widget>[],
      ),
      body: Consumer<AppState>(builder: (a, state, child) {
        if (state.scores.isEmpty) {
          return Center(
            child: Text(
                AppLocalizations.of(context)?.noScoreYet ?? 'No score yet'),
          );
        }
        return ListView(
          children: state.scores.map((e) {
            return const Text('test');
          }).toList(),
        );
      }),
    );
  }
}
