import 'package:beachcounter/models/appstate.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PlayersPage extends StatelessWidget {
  const PlayersPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text.rich(TextSpan(children: [
            TextSpan(text: AppLocalizations.of(context)?.players ?? 'Players'),
          ])),
          leading: IconButton(
              icon: const Icon(Icons.chevron_left),
              onPressed: () => Navigator.pop<void>(context)),
          actions: const <Widget>[],
        ),
        body: Consumer<AppState>(
          builder: (context, state, child) {
            return (state.allPlayers.isNotEmpty)
                ? ListView.builder(
                    itemCount: state.allPlayers.length,
                    padding: const EdgeInsets.symmetric(vertical: 16),
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                        leading: const Icon(Icons.person),
                        trailing: IconButton(
                          icon: const Icon(Icons.delete),
                          onPressed: () {
                            state.removePlayerAt(index);
                          },
                        ),
                        title: Text(
                          state.allPlayers[index],
                        ),
                      );
                    },
                  )
                : Center(
                    child: Text(AppLocalizations.of(context)?.noPlaysersYet ??
                        'No Players'));
          },
        ));
  }

  void onTap(String item) {}
}
