import 'package:beachcounter/models/appstate.dart';
import 'package:beachcounter/pages/scores.dart';
import 'package:beachcounter/widgets/app_drawer.dart';
import 'package:beachcounter/widgets/team.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    var appState = Provider.of<AppState>(context, listen: true);
    return Scaffold(
      drawer: const AppDrawer(),
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.menu),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
        title: (appState.gameOn)
            ? Text(
                '${AppLocalizations.of(context)?.game}: ${appState.playTime}')
            : const Text(''),
        centerTitle: true,
        actions: <Widget>[
          (appState.gameOn)
              ? IconButton(
                  icon: const Icon(Icons.replay_outlined),
                  tooltip: AppLocalizations.of(context)?.reset,
                  onPressed: () {
                    showRestartDialog(context, appState);
                  },
                )
              : Container(),
        ],
      ),
      floatingActionButton: Consumer<AppState>(
          builder: (context, state, child) => (state.showManualSideSwap)
              ? FloatingActionButton(
                  onPressed: () {
                    var appState =
                        Provider.of<AppState>(context, listen: false);
                    appState.swapTeams();
                  },
                  child: const Icon(Icons.swap_horizontal_circle_sharp),
                )
              : Container()),
      body: Center(
          child: Consumer<AppState>(
              builder: (context, state, child) => (state.gameOver)
                  ? gameOverPage(context, state)
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: createTeamWidgets(state),
                    ))),
    );
  }

  List<Widget> createTeamWidgets(state) {
    List<Widget> teams = [];
    for (var i = 0; i < state.teams.length; i++) {
      var tw = Expanded(
        child: TeamWidget(teamIdx: i),
      );
      teams.add(tw);
    }
    return teams;
  }

  showRestartDialog(BuildContext context, AppState appState) {
    Widget keepPlayers = TextButton(
      child: Text(AppLocalizations.of(context)?.keepPlayers ?? 'keep players'),
      onPressed: () async {
        Navigator.of(context).pop();
        if (appState.enableSaveGame) {
          await appState.saveGame();
        }
        appState.resetGame(keepPlayers: true);
      },
    );
    Widget cancelButton = TextButton(
      child: Text(AppLocalizations.of(context)?.cancel ?? 'Cancel'),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget newGameButton = TextButton(
      child: (appState.enableSaveGame)
          ? Text(AppLocalizations.of(context)?.newGame ?? 'newGame')
          : Text(AppLocalizations.of(context)?.delete ?? 'Delete'),
      onPressed: () async {
        Navigator.of(context).pop();
        if (appState.enableSaveGame) {
          await appState.saveGame();
        }
        appState.resetGame();
      },
    ); // set up the AlertDialog
    bool hasOneNameSet = appState.teams[0].players[0].hasNameSet ||
        appState.teams[0].players[1].hasNameSet ||
        appState.teams[1].players[0].hasNameSet ||
        appState.teams[1].players[1].hasNameSet;
    AlertDialog alert = AlertDialog(
      title: Text(AppLocalizations.of(context)?.newGame ?? 'new game'),
      content: (appState.enableSaveGame)
          ? Text(AppLocalizations.of(context)?.gameBeeingSaved ?? 'save game')
          : Text(AppLocalizations.of(context)?.deleteGame ?? 'delete game'),
      actions: (hasOneNameSet)
          ? [cancelButton, keepPlayers, newGameButton]
          : [cancelButton, newGameButton],
    ); // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  
  Widget gameOverPage(context, state) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
              '${AppLocalizations.of(context)?.winnerIsTeam} ${state.teams[state.winningTeam].name}',
              style: Theme.of(context).textTheme.headlineMedium),
          ListTile(
            title: Text(
                '${AppLocalizations.of(context)?.score}: ${state.teams[0].score} - ${state.teams[1].score}'),
          ),
          (state.teams[state.winningTeam].players[0].hasNameSet)
              ? ListTile(
                  title: Text(state.teams[state.winningTeam].players[0].name),
                )
              : Container(),
          (state.teams[state.winningTeam].players[1].hasNameSet)
              ? ListTile(
                  title: Text(state.teams[state.winningTeam].players[1].name),
                )
              : Container(),
          // ElevatedButton(
          //     onPressed: () {
          //       state.gameOver = false;
          //     },
          //     child: Text(
          //         AppLocalizations.of(context)?.continueGame ??
          //             'continue')),
          ElevatedButton(
              onPressed: () {
                state.resetGame();
              },
              child: Text(AppLocalizations.of(context)?.newGame ?? 'new game')),
          ElevatedButton(
              onPressed: () {
                state.resetGame(keepPlayers: true);
              },
              child: Text(
                  AppLocalizations.of(context)?.keepPlayers ?? 'keep players')),
          ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const Scoreboard()));
              },
              child: Text(
                  AppLocalizations.of(context)?.scoreBoard ?? 'scoreboard'))
        ],
      ),
    );
  }
}
