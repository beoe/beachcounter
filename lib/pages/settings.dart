import 'package:beachcounter/models/appstate.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    // var appstate = Provider.of<AppState>(context, listen: true);
    // return Consumer<AppState>(builder: (context, state, child) {
    // final prController = TextEditingController();
    // prController.text = state.project;
    return Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context)?.settings ?? 'Settings'),
          leading: IconButton(
              icon: const Icon(Icons.chevron_left),
              onPressed: () => Navigator.pop<void>(context)),
          actions: const <Widget>[],
        ),
        body: Consumer<AppState>(builder: (context, state, child) {
          return ListView(
            children: [
              // ListTile(
              //   leading: Switch(
              //       value: state.useOnline,
              //       onChanged: (val) {
              //         state.useOnline = val;
              //       }),
              //   title: Text('${AppLocalizations.of(context)?.useOnline}'),
              //   trailing: (state.isConnected)
              //       ? const Icon(Icons.wifi)
              //       : const Icon(Icons.wifi_off),
              // ),
              ListTile(
                  leading: Switch(
                      value: state.swapAllSeven,
                      onChanged: (val) {
                        state.swapAllSeven = val;
                      }),
                  title:
                      Text(AppLocalizations.of(context)?.swapAllSeven ?? '')),
              ListTile(
                  leading: Switch(
                      value: state.showManualSideSwap,
                      onChanged: (val) {
                        state.showManualSideSwap = val;
                      }),
                  title: Text(
                      AppLocalizations.of(context)?.showManualSideSwap ?? '')),
              ListTile(
                  leading: Switch(
                      value: state.showTimeFromLastPoint,
                      onChanged: (val) {
                        state.showTimeFromLastPoint = val;
                      }),
                  title: Text(
                      AppLocalizations.of(context)?.showTimeFromLastPoint ??
                          '')),
              ListTile(
                  leading: Switch(
                      value: state.vibrateOnPoint,
                      onChanged: (val) {
                        state.vibrateOnPoint = val;
                      }),
                  title:
                      Text(AppLocalizations.of(context)?.vibrateOnPoint ?? '')),
              // ListTile(
              //     leading: Switch(
              //         value: state.enableSaveGame,
              //         onChanged: (val) {
              //           state.enableSaveGame = val;
              //         }),
              //     title:
              //         Text(AppLocalizations.of(context)?.enableSaveGame ?? '')),
              ListTile(
                  leading: Switch(
                      value: state.showInlineCount,
                      onChanged: (val) {
                        state.showInlineCount = val;
                      }),
                  title: Text(
                      AppLocalizations.of(context)?.showInlineCount ?? '')),
            ],
          );
        }));
  }
}
