import 'package:beachcounter/models/appstate.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Scoreboard extends StatelessWidget {
  const Scoreboard({super.key});

  formatDur(Duration d) => d.toString().split('.').first.substring(2);

  @override
  Widget build(BuildContext context) {
    // return Consumer<AppState>(builder: (context, state, child) {
    // final prController = TextEditingController();
    // prController.text = state.project;
    return Scaffold(
      appBar: AppBar(
        title: Text('${AppLocalizations.of(context)?.scoreBoard}'),
        leading: IconButton(
            icon: const Icon(Icons.chevron_left),
            onPressed: () => Navigator.pop<void>(context)),
        actions: const <Widget>[],
      ),
      body: Consumer<AppState>(builder: (a, state, child) {
        if (state.scores.isEmpty) {
          return Center(
            child: Text(
                AppLocalizations.of(context)?.noScoreYet ?? 'No score yet'),
          );
        }
        return ListView(
            children: state.scores.map(
          (e) {
            var ts = DateTime.parse(e.ts);
            // var ago = timeago.format(ts);
            return ListTile(
              title: Text.rich(TextSpan(children: [
                TextSpan(text: (e.team == 0) ? 'A' : 'B'),
                const TextSpan(text: " - "),
                TextSpan(
                  text: e.servingPlayer == ''
                      ? '${AppLocalizations.of(context)?.player} ${e.servingPlayerIndex + 1}'
                      : e.servingPlayer,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                )
              ])),
              leading: Text.rich(TextSpan(children: [
                TextSpan(
                    text: '${e.scoreA}',
                    style: (e.team == 0)
                        ? const TextStyle(fontWeight: FontWeight.bold)
                        : const TextStyle(fontWeight: FontWeight.normal)),
                const TextSpan(text: ':'),
                TextSpan(
                    text: '${e.scoreB}',
                    style: (e.team == 1)
                        ? const TextStyle(fontWeight: FontWeight.bold)
                        : const TextStyle(fontWeight: FontWeight.normal)),
              ])),
              // trailing: Text('${ts.hour}:${ts.minute}:${ts.second}'),
              trailing: Text(ts.toIso8601String().substring(11, 19)),
              tileColor: (e.isSideout)
                  ? Theme.of(context).colorScheme.background
                  : Theme.of(context).colorScheme.secondaryContainer,
            );
          },
        ).toList());
      }),
    );
  }
}
