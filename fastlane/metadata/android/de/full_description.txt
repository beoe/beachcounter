**Beachvolleyball Counter**

Mit dieser einfachen App kannst du den Stand eines Beachvolleyball Spiels
protokollieren. Die App kümmert sich um die Service-Reihenfolge und kann auf
Wunsch den automatischen Seitenwechsel nach 7 Punkten grafisch darstellen.

